var nill = 0;

//Call this function PRIOR to calling any other function
//This will START A COMMUNICATION SESSION between LMS and a SCO
function Initialize()
{
	nill = 0;
	loadPage();
	//alert("his")
}

function CloseCourse()
	{
		var w = window;

		while (true)
		{
			if (w.location.href != w.parent.location.href)
			{
				w = w.parent;
			}
			else
			{
				break;
			}
		}

		w.close();
	}

//This function must be called at the end - meaning - NO other method SHALL be called after this method.
//The parameter - lesson_status indicates the status of the current learning unit (page)
function Exit(lesson_status)
{
	if (nill=="1") {
		value1="";
		SetSuspendedData(value1);
	}
	if (lesson_status=="completed" || lesson_status=="passed" || lesson_status=="failed")
	{
		doContinue(lesson_status);
	}
	else if (lesson_status=="incomplete")
	{
		doBack();
	}
	//alert(lesson_status+" from exit function")
}

function ReportScore(obtained, maximum, minimum)
{
	doLMSSetValue("cmi.core.score.raw", obtained);
	doLMSSetValue("cmi.core.score.max", maximum);
	doLMSSetValue("cmi.core.score.min", minimum);
//	alert("marks:"+obtained)
}

function SetSuspendedData(data)
{
	doLMSSetValue("cmi.suspend_data", data);
	//alert("setting\n\n"+data);
}

function GetSuspendedData()
{
	var str = doLMSGetValue("cmi.suspend_data");
	//var str=5;
	//alert("getting\n\n"+str);
	return str;
}

function GetLessonStatus()
{
	return doLMSGetValue("cmi.core.lesson_status");
	//alert(doLMSGetValue("cmi.core.lesson_status")+" lesson_status")
}

function GetStudentName()
{
	var str1 = doLMSGetValue("cmi.core.student_name");
	//alert("Name\n\n"+str1);
	return str1;
}

function GetStudentScore()
{
	return doLMSGetValue("cmi.core.score.raw");
}